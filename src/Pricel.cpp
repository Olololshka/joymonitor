/*
 * Pricel.cpp
 *
 *  Created on: 04.06.2012
 *      Author: tolyan
 */

#include <QPainter>
#include <QPen>
#include <QBrush>

#include "Pricel.h"

Pricel::Pricel(QGraphicsItem *parent) :
    QAbstractGraphicsShapeItem(parent)
{
    lines[0] = QLine(-PRICEL_SIZE / 2, 0, -PRICEL_SIZE / 2 + PRICEL_WINDOW, 0);
    lines[1] = QLine(0, -PRICEL_SIZE / 2, 0, -PRICEL_SIZE / 2 + PRICEL_WINDOW);
    lines[2] = QLine(PRICEL_WINDOW, 0, PRICEL_SIZE / 2, 0);
    lines[3] = QLine(0, PRICEL_WINDOW, 0, PRICEL_SIZE / 2);

    lines[4] = QLine(-PRICEL_WINDOW, 0, 0, -PRICEL_WINDOW);
    lines[5] = QLine(0, -PRICEL_WINDOW, PRICEL_WINDOW, 0);
    lines[6] = QLine(PRICEL_WINDOW, 0, 0, PRICEL_WINDOW);
    lines[7] = QLine(0, PRICEL_WINDOW, -PRICEL_WINDOW, 0);
}

Pricel::~Pricel()
{

}

QRectF Pricel::boundingRect() const
{
    return QRectF(-PRICEL_SIZE / 2, -PRICEL_SIZE / 2, PRICEL_SIZE, PRICEL_SIZE);
}

void Pricel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                   QWidget *widget)
{
    painter->setPen(this->pen());
    painter->setBrush(this->brush());
    for (unsigned int i = 0; i < 8; ++i)
        painter->drawLine(lines[i]);
}



