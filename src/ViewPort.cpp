/*
 * VievPort.cpp
 *
 *  Created on: 23.01.2013
 *      Author: tolyan
 */

#include <QPixmap>
#ifdef _GL
#include <QGLWidget>
#endif


#include "ViewPort.h"

ViewPort::ViewPort(QGraphicsScene *scene, QWidget *parent) :
    QGraphicsView(scene, parent)
{
    QPixmap BackgroundFragment(QSize(10, 10));
    BackgroundFragment.fill(Qt::black);
    setBackgroundBrush(BackgroundFragment);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
#ifdef _GL
    setViewport(new QGLWidget);
#endif
}

void ViewPort::resizeEvent(QResizeEvent* e)
{
    QGraphicsView::resizeEvent(e);
    fitInView(sceneRect(), Qt::KeepAspectRatio);
}
