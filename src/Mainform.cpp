//-----------------------------------------------------------------
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QApplication>
#include <QString>
#include <QLabel>
#include <QVBoxLayout>
#include <QPoint>
#include <QStatusBar>
#include <QSlider>
#include <QToolBar>
#include <QStyle>
#include <QRect>
//-----------------------------------------------------------------
#include <QInputDialog>
#include <QMessageBox>
#include <QDesktopWidget>
//-----------------------------------------------------------------
#include <qmodbus/common.h>
#include <qmodbus/ModbusEngine.h>
#include <mylog/mylog.h>
//-----------------------------------------------------------------
#include "main.h"
#include "MyScene.h"
#include "MainformStatusBar.h"
#include "ViewPort.h"
#include "Joy.h"
//-----------------------------------------------------------------
#include "Mainform.h"

Mainform::Mainform()
{
    //setAttribute(Qt::WA_DeleteOnClose);
    ReadSettings();

    createActions();
    createMenus();
    createToolBar();
    createObjects();

    resize(1024, 768);
    // в центр монитора
    setGeometry(
                QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(),
                                    qApp->desktop()->availableGeometry()));

    move(
                QApplication::desktop()->availableGeometry().center()
                - this->rect().center());

    showNormal();
}

Mainform::~Mainform()
{
}

bool Mainform::isConnected() const
{
    return connectAction->isChecked();
}

void Mainform::createMenus()
{
    fileMenu = menuBar()->addMenu(trUtf8("&Файл"));
    fileMenu->addAction(connectAction);
    connectAction->setCheckable(true);
    fileMenu->addSeparator();

    fileMenu->addSeparator();
    fileMenu->addAction(quitAction);

    optionsMenu = menuBar()->addMenu(trUtf8("&Настройки"));
    optionsMenu->addAction(connectionOptionsAction);
    optionsMenu->addAction(clearAction);
    optionsMenu->addSeparator();
    optionsMenu->addAction(InfTailAction);
}

void Mainform::createToolBar(void)
{
    toolbar = new QToolBar();
    toolbar->addAction(connectAction);
    toolbar->addAction(quitAction);
    toolbar->addSeparator();
    toolbar->addAction(connectionOptionsAction);
    toolbar->addSeparator();
    toolbar->addAction(clearAction);
    toolbar->addAction(InfTailAction);
    addToolBar(Qt::TopToolBarArea, toolbar);
}

void Mainform::createActions(void)
{
    connectAction = new QAction(trUtf8("&Подключиться"), this);
    connectAction->setIcon(QIcon(":/res/Connect"));
    connect(connectAction, SIGNAL(triggered()), this, SLOT(Connect()));
    quitAction = new QAction(trUtf8("&Выход"), this);
    quitAction->setIcon(QIcon(":/res/Quitturnof.png"));
    connect(quitAction, SIGNAL(triggered()), this, SLOT(AppQuit()));

    connectionOptionsAction = new QAction(trUtf8("Настройки &подключения"),
                                          this);
    connectionOptionsAction->setIcon(QIcon(":/res/Tools1.png"));
    connect(connectionOptionsAction, SIGNAL(triggered()), this,
            SLOT(ConnectionOptionsSetup()));

    clearAction = new QAction(trUtf8("&Очистить"), this);
    clearAction->setIcon(QIcon(":/res/clear.PNG"));

    InfTailAction = new QAction(trUtf8("Отображать бесконечную траекторию"), this);
    InfTailAction->setIcon(QIcon(":/res/INF.PNG"));
    InfTailAction->setCheckable(true);
    InfTailAction->setChecked(settings->value("WorkField/InfTail", false).toBool());
    connect(InfTailAction, SIGNAL(triggered(bool)), this, SLOT(SetInfTraectoryDraw(bool)));
}

void Mainform::Connect(bool connect)
{
    char res = 0;
    res += (char) joystic->Connect(connect);
    if (connect)
    {
        if (!res)
        {
            QMessageBox::critical(
                        this,
                        trUtf8("Ошибка подключения джойстика"),
                        trUtf8(
                            "Не получено ответа от джойстика.\n Убедитесь, что он подключен и установлены верные настройки."));
            connectAction->setChecked(false);
            ModbusCore->setAutoupdateGlobalEnabled(false);
            LOG_ERROR(trUtf8("Connected failed."));
        }
        else
        {
            ModbusCore->SetCycleDelay(
                        settings->value(trUtf8("Joy/SycleDelay"), 0).toULongLong());
            ModbusCore->setAutoupdateGlobalEnabled(true);
            LOG_INFO(trUtf8("Connected successful."));
        }
    }
    else
    {
        ModbusCore->SetCycleDelay(10);
        ModbusCore->setAutoupdateGlobalEnabled(false);
        LOG_INFO(trUtf8("Disconnected."));
        connectAction->setChecked(false);
    }
}

void Mainform::Connect()
{
    Connect(connectAction->isChecked());
}

void Mainform::ModbusError(int errcode)
{
    if ((errcode == 2) && (connectAction->isChecked()))
    {
        Connect(false);
        QMessageBox::critical(this, trUtf8("Ошибка подключения"),
                              trUtf8("Обнаружен разрыв соединения!"));
    }
}

void Mainform::AppQuit(void)
{
    qApp->quit();
}

void Mainform::SetInfTraectoryDraw(bool enabled)
{
    (*settings)["WorkField/InfTail"] = enabled;
    if (!enabled)
        Scene->clear();
}

void Mainform::ConnectionOptionsSetup(void)
{
    bool ok;
    PortName = QInputDialog::getText(this, trUtf8("Подключиться через..."),
                                     trUtf8("Введите имя COM-порта, устройству: "), QLineEdit::Normal,
                                     PortName, &ok);
    if (!ok)
        return; //cancel
    if (!PortName.isEmpty())
        (*settings)["Global/Port"] = PortName;

    ModbusCore->RestartConnection();
}

void Mainform::createObjects(void)
{
    Scene = new MyScene(this);
    Viewport = new ViewPort(Scene, this);
    setCentralWidget(Viewport);
    StatusBar = statusBar();
    StatusBarContent = new MainformStatusBar(StatusBar, StatusBar);
    joystic = new Joy(
                ModbusCore,
                settings->value(trUtf8("Global/ModbusAdress"),
                                _JOY_MB_DEFAULT_ADRESS).toUInt(), this);

    connect(clearAction, SIGNAL(triggered()), Scene, SLOT(clear()));
    connect(MyLog::LOG, SIGNAL(NewText(const QString&)), StatusBarContent,
            SLOT(SetMessage(const QString&)));
    connect(joystic, SIGNAL(currentPositionChanged(const QPointF&)), Scene,
            SLOT(SetCorrentPos(QPointF)));
    connect(joystic, SIGNAL(currentPositionChanged(const QPointF&)),
            StatusBarContent, SLOT(SetCurentPos(QPointF)));
    connect(ModbusCore, SIGNAL(ModbusGeneralFailure(int)), this,
            SLOT(ModbusError(int)));
}

void Mainform::ReadSettings(void)
{
    PortName = (*settings)["Global/Port"].toString();
}
