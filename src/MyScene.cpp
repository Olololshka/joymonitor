/*
 * MyScene.cpp
 *
 *  Created on: 20.03.2012
 *      Author: tolyan
 */

#include <cmath>

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLineItem>
#include <QPen>
#include <QRect>
#include <QRectF>
#include <QBrush>
#include <QSize>
#include <QPoint>
#include <QPixmap>
#include <QPainter>
#include <QGraphicsView>

#include <qmodbus/common.h>
#include <mylog/mylog.h>

#include "main.h"
#include "Pricel.h"

#include "MyScene.h"

#define SCALE_FACTOR        (100.0)
#define TRAECTORY_TO_BITMAP (50)
#define COLOR_SYCLE_LEN_MS  (1 * 60 * 1000)

MyScene::MyScene(QObject* parent) :
    QGraphicsScene(parent)
{
    SceneBorder = NULL;
    TraectoryLength = settings->value(trUtf8("WorkField/TraectoryLength"),
                                      MAX_POINTS_IN_VIEW).toUInt();

    bitmap = NULL;
    scencePixmap = NULL;
    updateMoveToBitmapCounter = 0;

    SetWorkspace();
    CreateObjects();
    clear();
}

void MyScene::SetWorkspaceRadius(unsigned int R)
{
    if (R == 0)
        R = settings->value(trUtf8("WorkField/Radius"),
                            _MY_SCENSE_DEFAULT_RADIUS).toUInt();
    SetWorkspace(QRect(-R, -R, R * 2, R * 2), 'e');
}

void MyScene::SetWorkspace(const QRect& NewWorkspace, char type)
{
    if (NewWorkspace == QRect(0, 0, 0, 0))
    { //читаем настройки
        type =
                settings->value(trUtf8("WorkField/Type"), "r").toString()[0].toLatin1();
        switch (type)
        {
        case 'e':
            SetWorkspaceRadius();
            return;
        case 'r':
        default:
            WorkSpace =
                    QRect(
                        QPoint(
                            settings->value(trUtf8("WorkField/XZero"),
                                            0).toInt(),
                            settings->value(trUtf8("WorkField/YZero"),
                                            0).toInt()),
                        QSize(
                            settings->value(trUtf8("WorkField/Width"),
                                            DEFAULT_WORK_SPACE_SIZE).toUInt(),
                            settings->value(trUtf8("WorkField/Height"),
                                            DEFAULT_WORK_SPACE_SIZE).toUInt()));
            break;
        }
    }
    else
    {
        WorkSpace = NewWorkspace;

        // сохраниеть настройки
        (*settings)["WorkField/XZero"] = NewWorkspace.x();
        (*settings)["WorkField/YZero"] = NewWorkspace.y();

        (*settings)["WorkField/Width"] = NewWorkspace.width();
        (*settings)["WorkField/Height"] = NewWorkspace.height();
    }
    if (SceneBorder != NULL)
    {
        SCENE_REMOVE(SceneBorder);
        __DELETE(SceneBorder);
    }
    switch (type)
    {
    case 'e':
    {
        QGraphicsEllipseItem* t = new QGraphicsEllipseItem;
        t->setPen(QPen(Qt::yellow));
        t->setBrush(QBrush(Qt::NoBrush));
        t->setRect(WorkSpace.x(), WorkSpace.y(), WorkSpace.width(),
                   WorkSpace.height());
        SceneBorder = t;
    }
        break;
    case 'r':
    default:
    {
        QGraphicsRectItem* t = new QGraphicsRectItem;
        t->setPen(QPen(Qt::yellow));
        t->setBrush(QBrush(Qt::NoBrush));
        t->setRect(WorkSpace.x(), WorkSpace.y(), WorkSpace.width(),
                   WorkSpace.height());
        SceneBorder = t;
    }
        break;
    }

    LOG_INFO(
                trUtf8("New workspace settings: [Xz: %1, Yz: %2, W: %3, H: %4]").arg( WorkSpace.x()).arg(WorkSpace.y()).arg(WorkSpace.width()).arg( WorkSpace.height()));

    if (bitmap != NULL)
        __DELETE(bitmap);
    bitmap = new QPixmap(QSize(WorkSpace.size() * SCALE_FACTOR));
    bitmap->fill(Qt::black);

    setSceneRect(WorkSpace);
}

MyScene::~MyScene()
{
    if (bitmap != NULL)
        __DELETE(bitmap);
}

void MyScene::clear()
{
    SCENE_REMOVE(SceneBorder);
    SCENE_REMOVE(horLine);
    SCENE_REMOVE(vertLine);
    SCENE_REMOVE(CurrentPosition);
    if (scencePixmap != NULL)
    {
        SCENE_REMOVE(scencePixmap);
        __DELETE(scencePixmap);
        bitmap->fill(Qt::black);
    }
    linesQueue.clear();
    //---------------
    QGraphicsScene::clear();
    //---------------
    addItem(SceneBorder);
    addItem(horLine);
    addItem(vertLine);
    addItem(CurrentPosition);
    scencePixmap = addPixmap(*bitmap);
    scencePixmap->scale(1 / SCALE_FACTOR, 1 / SCALE_FACTOR);
    QSizeF scencePixmapScaledSize = scencePixmap->boundingRect().size() / SCALE_FACTOR;
    scencePixmap->setPos(-scencePixmapScaledSize.height() / 2, -scencePixmapScaledSize.width() / 2);
    scencePixmap->setZValue(-2);
    StartTime = QDateTime::currentDateTime();
}

void MyScene::SetCorrentPos(QPointF newPos)
{
    if (!WorkSpace.contains(newPos.toPoint()))
    {
        LOG_DEBUG(
                    trUtf8("Point (%1, %2) if out of workspace!").arg(newPos.x()).arg( newPos.y()));
    }
    else
    {
        bool InfTail = settings->value("WorkField/InfTail", false).toBool();
        CurrentPosition->setPos(newPos);
        QGraphicsLineItem* line;
        if (linesQueue.isEmpty())
            line = addLine(newPos.x(), newPos.y(), newPos.x(), newPos.y(), QPen(Qt::red));
        else
        {
            QLineF _line = linesQueue.last()->line();
            line = addLine(_line.x2(), _line.y2(), newPos.x(), newPos.y(),
                           QPen(Qt::red));
        }
        linesQueue.append(line);

        if (InfTail)
        {
            if (!(updateMoveToBitmapCounter = (updateMoveToBitmapCounter + 1) % TRAECTORY_TO_BITMAP))
            {
                // очистить траекторию, и прерисовать её в битмап.
                QList<QGraphicsLineItem*>::iterator it = linesQueue.begin();
                QList<QGraphicsLineItem*>::iterator end = linesQueue.end();
                QPainter painter(bitmap);
                painter.setPen(QPen(PaintColor()));
                if (views().size() != 0)
                    views().at(0)->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
                for (;it < end - 1; ++it)
                {
                    QLineF ScaledLine = (*it)->line();
                    ScaledLine.setPoints(QPointF(ScaledLine.x1() * SCALE_FACTOR,
                                                 ScaledLine.y1() * SCALE_FACTOR),
                                         QPointF(ScaledLine.x2() * SCALE_FACTOR,
                                                 ScaledLine.y2() * SCALE_FACTOR));

                    painter.drawLine(ScaledLine.x1() + bitmap->size().width() / 2,
                                     ScaledLine.y1() + bitmap->size().height() / 2,
                                     ScaledLine.x2() + bitmap->size().width() / 2,
                                     ScaledLine.y2() + bitmap->size().height() / 2);
                    SCENE_REMOVE((*it));
                    __DELETE((*it));
                }
                QGraphicsLineItem* lastline = linesQueue.last();
                linesQueue.clear();
                linesQueue.append(lastline);
                scencePixmap->setPixmap(*bitmap);
                if (views().size() != 0)
                    views().at(0)->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
            }
        }

        if (linesQueue.size() > TraectoryLength)
        {
            line = linesQueue.takeFirst();
            SCENE_REMOVE(line);
        }
        if (!InfTail)
            updateColor();
    }
}

void MyScene::CreateObjects(void)
{
    //перекрестье
    horLine = new QGraphicsLineItem;
    vertLine = new QGraphicsLineItem;
    QPen whitePen(Qt::white);
    QBrush solidBrush(Qt::SolidPattern);
    horLine->setPen(whitePen);
    vertLine->setPen(whitePen);
    horLine->setLine(WorkSpace.x(), 0, WorkSpace.x() + WorkSpace.height(), 0);
    vertLine->setLine(0, WorkSpace.y(), 0, WorkSpace.y() + WorkSpace.width());

    CurrentPosition = new Pricel;
    QPen CurrentPositionPen;
    CurrentPositionPen.setColor(Qt::green);
    CurrentPositionPen.setWidthF(1.5);
    CurrentPosition->setPen(CurrentPositionPen);
    CurrentPosition->setBrush(QBrush(Qt::green, Qt::SolidPattern));
    qreal factor = CurrentPosition->boundingRect().size().height()
            / WorkSpace.size().height() * PRICEL_SCALEFACTOR_CONST
            * WorkSpace.size().width();
    CurrentPosition->scale(factor, factor);
}

//http://stackoverflow.com/questions/8208905/hsv-0-255-to-rgb-0-255
//HSL->RGB
//HSL [0..1]
//RGB [0..255]
// h - оттенок (аргумент)
// s ставим на максимум, для получения "чистого" цвета
// v уменьшается к концу
//---------------
void MyScene::updateColor()
{
    QList<QGraphicsLineItem*>::iterator it = linesQueue.begin();
    for (float h = 0.75; it < linesQueue.end();
         h -= 0.75 / linesQueue.size(), ++it)
    {


        (*it)->setPen(QPen(QColor::fromHsvF(h, 1.0, 1.0 - h / 0.75)));
    }
}

QColor MyScene::PaintColor()
{
    QDateTime now = QDateTime::currentDateTime();
    uint64_t diff = now.daysTo(StartTime);
    diff += now.time().msecsTo(StartTime.time());
    diff %= COLOR_SYCLE_LEN_MS;
    float h = 1.0 / COLOR_SYCLE_LEN_MS * diff;
    return QColor::fromHsvF(h, 1.0, 1.0);
}
//----------------

bool MyScene::setTraectoryLength(unsigned int length)
{
    TraectoryLength = length;
    (*settings)["WorkField/TraectoryLength"] = TraectoryLength;
    return true;
}
