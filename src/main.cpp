//-----------------------------------------------------------------
#include <QApplication>
#include <QThread>
#include <QAction>
#include <QString>
//-----------------------------------------------------------------
#include <qmodbus/common.h>
#include <qmodbus/Sleeper.h>
#include <qmodbus/ModbusEngine.h>
#include <qmodbus/ModbusRequest.h>
#include <mylog/mylog.h>
//-----------------------------------------------------------------
#include "Settings.h"
#include "Mainform.h"
//-----------------------------------------------------------------
#ifdef _JS
#include <QScriptEngine>
#include "ScriptObject.h"
#endif
//-----------------------------------------------------------------
#ifdef _SCRIPT_DEBUG
#include <QScriptEngineDebugger>
#endif
//-----------------------------------------------------------------

/**
 * @mainpage
 * Программа JoyMonitor предоставляет визуализацию услия, приложенного к джойстику.
 * <br>Резистивный джойстик и контроллер управления соединяются с ПК через последовательный интерфейс RS485
 * <br>Программа написана с использование библиотек с открытым исходным кодом:
 *      - Qt : http://qt-project.org/
 *      - libmodbus : https://github.com/ololoshka2871/libmodbus
 *      - libqmodbus :
 *      - libmylog :
 *      - libsettingscmdline :
 * <br>Система сборки: cmake : http://www.cmake.org/
 * <br>Генерация документации: Doxygen : http://www.doxygen.org/
 * <br>Протестированo на:
 *      - MinGW (Windows) : http://www.mingw.org/
 *      - Linux
 */

Settings* settings;
ModbusEngine* ModbusCore;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("JoyMonitor");

    //настройко-держатель
    Settings _settings(argc, argv);
    settings = &_settings;

    //Лог
    MyLog::myLog _LOG;

    // синхронно-асинхронный движок Modbus
    ModbusEngine _ModbusCore;
    ModbusCore = &_ModbusCore;

    Mainform mainwindow; // Главная форма

    ModbusCore->start(QThread::LowPriority); // запускаем фоновый обработчик

    mainwindow.show();

    return app.exec();
}
//-----------------------------------------------------------------
