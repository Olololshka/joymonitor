/*
 * PerfTimeCounter.cpp
 *
 *  Created on: 30.05.2012
 *      Author: tolyan
 */

#include <sys/time.h>

#include <qmodbus/common.h>

#include "PerfTimeCounter.h"

PerfTimeCounter::PerfTimeCounter()
{

}

PerfTimeCounter::~PerfTimeCounter()
{
}

unsigned long PerfTimeCounter::GetTickCount(void)
{
    struct timeval tv;
#ifdef WIN32
    gettimeofday(&tv, NULL);
#else
    gettimeofday(&tv, (timezone*)NULL);
#endif
    return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

void PerfTimeCounter::Start(void)
{
    starttime = GetTickCount();
}

unsigned long PerfTimeCounter::Catch()
{
    unsigned long time = GetTickCount();
    unsigned long res = time - starttime;
    starttime = time;
    return res;
}

