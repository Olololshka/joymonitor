/*
 * MainformStatusBar.cpp
 *
 *  Created on: 26.03.2012
 *      Author: tolyan
 */

#include <QLabel>
#include <QHBoxLayout>
#include <QStatusBar>
#include <QPoint>
#include <QPointF>
#include <QApplication>

#include <qmodbus/common.h>

#include "MainformStatusBar.h"

MainformStatusBar* StatusBarContent;

MainformStatusBar::MainformStatusBar(QStatusBar* StatusBar, QWidget* parent) :
    QWidget(parent)
{
    MessageBox = new QLabel(this);
    XPos = new QLabel(trUtf8("X"), this);
    YPos = new QLabel(trUtf8("Y"), this);
    StatusBar->addWidget(MessageBox);
    StatusBar->addPermanentWidget(XPos);
    StatusBar->addPermanentWidget(YPos);
    devider = 0;
}

MainformStatusBar::~MainformStatusBar()
{
}

void MainformStatusBar::SetMessage(const QString& newMessage)
{
    MessageBox->setText(newMessage);
}

void MainformStatusBar::SetCurentPos(QPoint pos)
{
    XPos->setText(trUtf8("X: %1").arg(pos.x()));
    YPos->setText(trUtf8("Y: %1").arg(pos.y()));
}

void MainformStatusBar::SetCurentPos(QPointF pos)
{
    if ((devider = devider++ % 10) == 0)
    {
        XPos->setText(trUtf8("X: %1").arg(pos.x(), 10, 'f', 2));
        YPos->setText(trUtf8("Y: %1").arg(pos.y(), 10, 'f', 2));
    }
}
