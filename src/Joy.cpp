/*
 * Joy.cpp
 *
 *  Created on: 24.01.2013
 *      Author: tolyan
 */
#include <QPointF>
#include <QApplication>

#include <qmodbus/ModbusEngine.h>
#include <mylog/mylog.h>
#include <qmodbus/Sleeper.h>

#include "Joy.h"

Joy::Joy(ModbusEngine* mb, unsigned char adress, QObject* parent) :
    QObject(parent)
{
    connected = false;
    ModBus = mb;
    SetAdress(adress);
    connect(ModBus, SIGNAL(AutoupdateComplead(void)), this,
            SLOT(ParseData(void)));
#ifdef _SHOW_UPDATE_INTERVAL
    perf.Start();
#endif
}

Joy::~Joy()
{
}

bool Joy::Connect(bool connect)
{
    if (connect)
    { //подключение
        ModbusRequest* IDrequest = ModbusRequest::BuildReadHoldingRequest(
                    adress, _JOY_ID_ADRESS, 1);

        char i = 5; // три попытки соединения
        while (i--)
        {
            // если возникла ошибка (errno = -112335618), попробовать еще раз
            ModBus->SyncRequest(*IDrequest);
            if ((IDrequest->getErrorrCode() == ModbusRequest::ERR_OK) ||
                    (((int)IDrequest->getErrorrCode()) != -112335618))
                break;
            Sleeper::sleep_ms(300);
            QApplication::processEvents();
        }
        ModbusRequest::ErrorCode code = IDrequest->getErrorrCode();
        if (code == ModbusRequest::ERR_OK)
        {
            uint16_t redID = IDrequest->getAnsverAsShort().at(0);
            if (redID == _JOY_DEVICE_ID)
            {
                LOG_INFO(
                            trUtf8("Joystic device connect success, address: %1").arg(IDrequest->getDeviceAddress()));
                //enabling all requests
                QList<ModbusRequest*>::iterator it = requests.begin();
                for (; it < requests.end(); ++it)
                    (*it)->setEnabled(true);
                connected = true;
            }
            else
            {
                LOG_ERROR(
                            trUtf8("Incorrect device ID at address: %1 (%2)").arg(IDrequest->getDeviceAddress()).arg(redID, 4, 16, QChar('0')));
                connected = false;
            }
        }
        else
        {
            LOG_ERROR(
                        trUtf8("Error while data exchange with device address: %1").arg(IDrequest->getDeviceAddress()));
            connected = false;
        }
        delete IDrequest;
    }
    else
    { //отключение
        QList<ModbusRequest*>::iterator it = requests.begin();
        for (; it < requests.end(); ++it)
            (*it)->setEnabled(false);

        connected = false;
    }
    return connected;
}

void Joy::SetAdress(unsigned char adress)
{
    this->adress = adress;
    if (requests.empty())
    { //создание реквеста и добавление его в скисок автообновления
        requests.append(
                    ModbusRequest::BuildReadInputsRequest(adress,
                                                          _JOY_OUT_DATA_ADRESS, 4));
        QList<ModbusRequest*>::iterator it = requests.begin();
        for (; it < requests.end(); ++it)
            (*it)->setEnabled(false);
        ModBus->AddListOfAutocallRequests(requests);
    }
    else
    {
        QList<ModbusRequest*>::iterator it = requests.begin();
        for (; it < requests.end(); ++it)
            (*it)->setDeviceAddress(adress);
    }
}

void Joy::ParseData(void)
{
    if (!connected)
        return;
    if (requests.at(0)->getErrorrCode() == ModbusRequest::ERR_OK)
    {
        QList<float> data = requests.at(0)->getAnsverAsFloat();
        if (data.size() == 2)
            emit currentPositionChanged(QPointF(data.at(0), data.at(1)));
#ifdef _SHOW_UPDATE_INTERVAL
        LOG_DEBUG(trUtf8("Cycle time:%1").arg(perf.Catch()));
#endif
    }
}
