/*
 * MainformStatusBar.h
 *
 *  Created on: 26.03.2012
 *      Author: tolyan
 */

#ifndef MAINFORMSTATUSBAR_H_
#define MAINFORMSTATUSBAR_H_

#include <QWidget>

class QLabel;
class QStatusBar;
class QPoint;
class QPointF;

/**
 * @brief The MainformStatusBar class - класс, определяющий объекты на строки состояния (статус-бара) приложения
 */
class MainformStatusBar : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief MainformStatusBar - конструктор
     * @param StatusBar - указатель на статус-бар окна, на который будет происходить вывод
     * @param parent - "родитель"
     */
    MainformStatusBar(QStatusBar* StatusBar, QWidget* parent = NULL);

    /**
     * @brief ~MainformStatusBar - деструктор
     */
    virtual ~MainformStatusBar();

public slots:
    /**
     * @brief SetMessage - слот для установки нового текста в левой части статаус-бара
     * @param newMessage - текст сообщения
     */
    void SetMessage(const QString& newMessage);

    /**
     * @brief SetCurentPos - установка целочисленных координат в правой части статаус-бара
     * @param pos - целочисленные координаты, в формате QPoint
     */
    void SetCurentPos(QPoint pos);

    /**
     * @brief SetCurentPos - установка дробных координат в правой части статаус-бара
     * @param pos - дробные координаты, в формате QPointF
     */
    void SetCurentPos(QPointF pos);

private:
    QLabel* MessageBox;
    QLabel *XPos, *YPos;
    char devider;
};

extern MainformStatusBar* StatusBarContent;

#endif /* MAINFORMSTATUSBAR_H_ */
