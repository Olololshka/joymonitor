//-----------------------------------------------------------------
#ifndef MAINFORM_H
#define MAINFORM_H

#include <QMainWindow>

//--------------------
class QAclion;
class QMenu;
class QMenuBar;
class QApplication;
class QString;
class QLabel;
class QVBoxLayout;
class QHBoxLayout;
class QInputDialog;
class QStatusBar;
//---------------------
class MyScene;
class MainformStatusBar;
class libmodbusRAWWarper;
class QToolBar;
class QSlider;
class ViewPort;
class Joy;

/**
 * @brief The Mainform class - класс главной формы.
 * Определяем положения контролов на главной форме и объединяем основные компоненты
 */

class Mainform: public QMainWindow
{
    Q_OBJECT

    /**
      * @brief Статус подключения - подключен/отключен
      */
    Q_PROPERTY(bool connected READ isConnected WRITE Connect)

public:
    /**
     * @brief Mainform - конструктор
     */
    Mainform();

    /**
     * @brief ~Mainform - деструктор
     */
    ~Mainform();

    /**
     * @brief isConnected - геттер состояния подключения
     * @return true - если подключение сейчас активно
     */
    bool isConnected() const;

private:
    /**
     * @brief createActions - конструирует обьекты QAction для меню и тулбара
     */
    void createActions(void);

    /**
     * @brief createMenus - конструирует главное меню
     */
    void createMenus(void);

    /**
     * @brief createToolBar - конструирует статус-бар
     */
    void createToolBar(void);

    /**
     * @brief createObjects - конструирует обьекты Viewport, Scene, Joy и устанавливает связи сигнал-слот между ними
     */
    void createObjects(void);

    /**
     * @brief ReadSettings - читает настройки (имя COM-порта)
     */
    void ReadSettings(void);

    QAction *connectAction, *quitAction, *connectionOptionsAction, *clearAction, *InfTailAction;

    QMenu* fileMenu;
    QMenu* optionsMenu;

    QToolBar *toolbar;

    QString PortName;

    MyScene* Scene;

    ViewPort* Viewport;

    QStatusBar* StatusBar;

    Joy* joystic;

private slots:
    /**
     * @brief ConnectionOptionsSetup - Слот, диалогового окна, предлогающего сменить настройку COM-порта
     */
    void ConnectionOptionsSetup(void);

    /**
     * @brief Connect - слот, начинающий процедуру подключения устройств modbus
     */
    void Connect();

    /**
     * @brief ModbusError - слот, отображающий ошибку в случае обрыва соединения modbus
     * @param errcode - код ошибки modbus
     */
    void ModbusError(int errcode);

public slots:
    /**
     * @brief Connect - слот, аналогичный Connect(), но для внешнего использования
     * @param connect - true - начать процидуру подключения, false - разорвать соединение
     */
    void Connect(bool connect);

    /**
     * @brief AppQuit - Слот, вызов которго завершает работу программы
     */
    void AppQuit(void);

    /**
     * @brief SetInfTraectoryDraw - Слот, включающий режим рисования бесконечной траектории
     * @param enabled - включить/выключить рисование бесконечной траектории
     */
    void SetInfTraectoryDraw(bool enabled);
};
//-----------------------------------------------------------------
#endif
//-----------------------------------------------------------------
