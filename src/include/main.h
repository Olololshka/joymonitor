/*
 * main.h
 *
 *  Created on: 24.05.2012
 *      Author: tolyan
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <QApplication>

#include <qmodbus/ModbusEngine.h>

#include "Settings.h"

extern Settings* settings;
extern ModbusEngine* ModbusCore;

#endif /* MAIN_H_ */
