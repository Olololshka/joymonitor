/*
 * Joy.h
 *
 *  Created on: 24.01.2013
 *      Author: tolyan
 */

#ifndef JOY_H_
#define JOY_H_

#include <QObject>
#include <QList>
#ifdef _SHOW_UPDATE_INTERVAL
#include "PerfTimeCounter.h"
#endif

/// Уникальный идентификатор устройства
#define _JOY_DEVICE_ID          (0xDBD0)

/// адрес устройства на шине modbus
#define _JOY_MB_DEFAULT_ADRESS	(1)

/// адрес регистра идентификатора
#define _JOY_ID_ADRESS          (0x0000)

/// начальный регистр блока данных джойстика
#define _JOY_OUT_DATA_ADRESS	(0x0000)

class QPointF;
class ModbusRequest;
class ModbusEngine;

/**
 * @brief The Joy class - Класс предоставляет основные методы для получения данных с джойстика
 */

class Joy: public QObject
{
    Q_OBJECT
public:

    /**
     * @brief Joy - конструктор
     * @param mb - указатель на экземпляр класса ModbusEngine, через который будет происходить обмен
     * @param adress - адрес джойстика на шине modbus
     * @param parent - "родитель"
     */
    Joy(ModbusEngine* mb, unsigned char adress, QObject* parent = NULL);

    /**
     * @brief ~Joy - деструктор
     */
    virtual ~Joy();

    /**
     * @brief Connect - "выключатель"
     * @param connect - если 'true' - начинает чтение данных из джойстика, иначе останавливает чтение
     * @return true в случае удачного подключения
     */
    bool Connect(bool connect);

    /**
     * @brief SetAdress - установить/сменить адрес modbus к которому происходит подключение
     * @param adress - новый адрес modbus
     */
    void SetAdress(unsigned char adress);


signals:
    /**
     * @brief currentPositionChanged - сигнал, испускаемвый каждый раз, когда прочитана новая порция данных
     * @param _t1 - новые значения X и Y, упакованные в QPointF
     */
    void currentPositionChanged(const QPointF&);

private:
    /**
     * @brief connected - флаг состояния "подключен"
     */
    bool connected;

    /**
     * @brief adress - адрес, с которым происходит обмен
     */
    unsigned char adress;

    /**
     * @brief ModBus - указатель на ModbusEngine, через который происходит обмен
     */
    ModbusEngine* ModBus;

    /**
     * @brief requests - список указателей на запросы типа ModbusRequest, через которые происходит обмен данными джойстик <-> ПК
     */
    QList<ModbusRequest*> requests;

#ifdef _SHOW_UPDATE_INTERVAL
    /**
     * @brief perf - Объект таймера, для определения премени задережек (только для отладки)
     */
    PerfTimeCounter perf;
#endif

private slots:
    /**
     * @brief ParseData - слот, вызываемвый обьектом ModbusEngine по завершении цыкла обновления данных.
     */
    void ParseData(void);
};

#endif /* JOY_H_ */
