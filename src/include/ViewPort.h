/*
 * ViewPort.h
 *
 *  Created on: 23.01.2013
 *      Author: tolyan
 */

#ifndef VIEWPORT_H_
#define VIEWPORT_H_

#include <QGraphicsView>

/**
 * @brief The ViewPort class - класс графического представления
 * Унаследован от QGraphicsView и включает поддержку OpenGL
 */
class ViewPort : public QGraphicsView
{
    Q_OBJECT
public:
    /**
     * @brief ViewPort - конструктор
     * @param scene - виртуальная сцена
     * @param parent - "родитель"
     */
    ViewPort(QGraphicsScene *scene, QWidget *parent = 0);

protected:
    /**
     * @brief resizeEvent - метод, отвечающий за автоматическое масштабирование содержимого сцены под размер виджета
     * при измененеиях размера окна
     * @param e - указатель на объект-событие изменения размера
     */
    void resizeEvent(QResizeEvent* e);
};

#endif /* VIEWPORT_H_ */
