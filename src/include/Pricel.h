/*
 * Pricel.h
 *
 *  Created on: 04.06.2012
 *      Author: tolyan
 */

#ifndef PRICEL_H_
#define PRICEL_H_

#include <QAbstractGraphicsShapeItem>

#define PRICEL_SIZE	(20)
#define PRICEL_WINDOW	(5)

class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QRectF;
class QGraphicsItem;

/**
 * @brief The Pricel class - клас, рисующий прицел
 * Рисует обьект-прицел на виртуальной сцене
 */
class Pricel: public QAbstractGraphicsShapeItem
{
public:
    /**
     * @brief Pricel - конструктор
     * @param parent - Графический объект "родитель"
     */
    Pricel(QGraphicsItem *parent = 0);

    /**
     * @brief ~Pricel - деструктор
     */
    virtual ~Pricel();

    /**
     * @brief boundingRect - Вычислить описывающий прямоугольник
     * @return Описывающий прямоугольник (необходимо для реализации внутренних механизмом QGraphicsScene)
     */
    QRectF boundingRect() const;

    /**
     * @brief paint - перерисовать себя
     * @param painter - указарель на контекст рисования
     * @param option - опции рисования (не используется)
     * @param widget - указатель на виджет на котором происходит рисование (не используется)
     */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

private:
    /// массив координат отрезков из которых состоит прицел
    QLine lines[8];
};

#endif /* PRICEL_H_ */
