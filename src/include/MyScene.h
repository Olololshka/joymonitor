/*
 * MyScene.h
 *
 *  Created on: 20.03.2012
 *      Author: tolyan
 */

#ifndef MYSCENE_H_
#define MYSCENE_H_

#include <QGraphicsScene>
#include <stdint.h>
#include <assert.h>
#include <QColor>
#include <QDateTime>

/// радиус окружности сцены по умолчанию
#define _MY_SCENSE_DEFAULT_RADIUS	(1)

/// размер сцены по умолчанию
#define DEFAULT_WORK_SPACE_SIZE		(10)

/// масштабный коэфициент рисования прицела
#define PRICEL_SCALEFACTOR_CONST	(0.0005)

/// длина "хвоста" по умолчанию
#define MAX_POINTS_IN_VIEW		(50)

/// макрос удаления объекта со сцены
#define SCENE_REMOVE(obj)		{\
    assert(obj != NULL);\
    if (obj->scene() == this) \
    removeItem(obj); \
    }

class QGraphicsSceneMouseEvent;
class QPoint;
class QSize;
class QRectF;
class Pricel;
class QGraphicsEllipseItem;
class QGraphicsPixmapItem;
class QPixmap;

/**
 * @brief The MyScene class - класс виртуальной сцены
 */
class MyScene: public QGraphicsScene
{
    Q_OBJECT
public:
    /**
     * @brief MyScene - конструктор
     * @param parent - "родитель"
     */
    MyScene(QObject* parent = NULL);

    /**
     * @brief ~MyScene - деструктор
     */
    virtual ~MyScene();

    /**
     * @brief setTraectoryLength - устанавливает длину "хвоста" (гаснущей траектории) в точках
     * @param length - новая длина (<= 1000)
     * @return true в случае удачи
     */
    bool setTraectoryLength(unsigned int length);

public slots:
    /**
     * @brief SetCorrentPos - слот, устанавливающий новое положение текущей точки. При этом последняя точка хвоста исчезает.
     * @param newCurrentPos - новая точка в формате QPointF
     */
    void SetCorrentPos(QPointF newCurrentPos = QPointF(0, 0));

    /**
     * @brief SetWorkspace - устанавливает новый размер рабочей области
     * @param NewWorkspace - прямоугольник, в который заключена радочая область
     * @param type - 'r' - область прямоугольная, 'e' - обоасть элиптическая, вписана в прямоугольник
     */
    void SetWorkspace(const QRect& NewWorkspace = QRect(0, 0, 0, 0), char type = 'r');

    /**
     * @brief SetWorkspace - пергруженный метод, задает примоугольник напрямую
     * @param X Y - вершина прямоугольника
     * @param W - ширина
     * @param H - высота
     * @param type - 'r' - область прямоугольная, 'e' - обоасть элиптическая, вписана в прямоугольник
     */
    inline void SetWorkspace(int X, int Y, int W, int H, char type = 'r')
    {
        SetWorkspace(QRect(X, Y, W, H), type);
    }

    /**
     * @brief SetWorkspaceRadius - установить рабочую область как круг с центром 0, 0
     * @param R - радиус круга
     */
    void SetWorkspaceRadius(unsigned int R = 0);

    /**
     * @brief clear - очистить рабочую область. Удаляет "хвост"
     */
    void clear();

private:
    QRect WorkSpace;
    Pricel* CurrentPosition;
    QGraphicsItem* SceneBorder;
    QList<QGraphicsLineItem*> linesQueue;

    QColor color;
    QGraphicsLineItem *horLine, *vertLine;
    char borderType;
    unsigned int TraectoryLength;
    QPixmap *bitmap;
    QGraphicsPixmapItem* scencePixmap;

    unsigned int updateMoveToBitmapCounter;

    QDateTime StartTime;

    /**
     * @brief CreateObjects - создает оси и границу рабочей области, создает прицел
     */
    void CreateObjects(void);

    /**
     * @brief updateColor - перерисовывает "хвост" градиентом от красного до синего.
     */
    void updateColor();

    /**
     * @brief PaintColor - вычисляет новый цвет для куска длинной траектории циклично от времени начала.
     * @return цвет отрезка.
     */
    QColor PaintColor();
};

#endif /* MYSCENE_H_ */
