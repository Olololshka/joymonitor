# - Try to find libmylig
# Once done this will define
#
#  MYLOG_FOUND - system has MYLOG
#  MYLOG_INCLUDE_DIR - the MYLOG include directory
#  MYLOG_LIBRARIES - Link these to use MYLOG

# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if (MYLOG_INCLUDE_DIR AND MYLOG_LIBRARIES)

  # in cache already
  set(SETTINGSCMDLINE_FOUND TRUE)
  message(STATUS "Found libmylog: ${MYLOG_LIBRARIES}")

else (MYLOG_INCLUDE_DIR AND MYLOG_LIBRARIES)

  find_path(MYLOG_INCLUDE_DIR mylog.h
        PATH_SUFFIXES mylog
	PATHS
		/usr/include
		/usr/local/include
		$ENV{WD}/../include
		$ENV{WD}/../local/include
  )

  find_library(MYLOG_LIBRARIES NAMES mylog
        PATHS
                /usr/lib
		/usr/bin
		/usr/local/lib
		/usr/local/bin
  )

 set(CMAKE_REQUIRED_INCLUDES ${MYLOG_INCLUDE_DIR})
 set(CMAKE_REQUIRED_LIBRARIES ${MYLOG_LIBRARIES})

   if(MYLOG_INCLUDE_DIR AND MYLOG_LIBRARIES)
    set(SETTINGSCMDLINE_FOUND TRUE)
  else (MYLOG_INCLUDE_DIR AND MYLOG_LIBRARIES)
    set(SETTINGSCMDLINE_FOUND FALSE)
  endif(MYLOG_INCLUDE_DIR AND MYLOG_LIBRARIES)

  if (SETTINGSCMDLINE_FOUND)
    FILE(TO_NATIVE_PATH ${MYLOG_LIBRARIES} MYLOG_LIBRARIES)
    FILE(TO_NATIVE_PATH ${MYLOG_INCLUDE_DIR} MYLOG_INCLUDE_DIR)
    if (NOT SETTINGSCMDLINE_FIND_QUIETLY)
      message(STATUS "Found libmylog: ${MYLOG_LIBRARIES}")
    endif (NOT SETTINGSCMDLINE_FIND_QUIETLY)
  else (SETTINGSCMDLINE_FOUND)
    if (SETTINGSCMDLINE_FIND_REQUIRED)
      message(FATAL_ERROR "libmylog not found. Please install it.")
    endif (SETTINGSCMDLINE_FIND_REQUIRED)
  endif (SETTINGSCMDLINE_FOUND)

  mark_as_advanced(MYLOG_INCLUDE_DIR MYLOG_LIBRARIES)

endif (MYLOG_INCLUDE_DIR AND MYLOG_LIBRARIES)
